# DemocracyHelper frontend

## Deployment

Set up a directory 'settings' and configure the file settings/appsettings.json similar to the example at /settingsExample/appsettings.json.

**Remember to double check the backendUrl!**

    docker build -t dh_frontend_img .
    docker run --rm -d -p 3001:3000 --name dh_frontend dh_frontend_img

Alternative

    docker compose up

Internal network (set up .env similar to example.env)

    docker compose -f docker-compose.internal_network.yml up -d


## Staging

thehowtoproject.ch/democracy_helper


## Testing

Proposals add, update, delete

Votes add, update, delete

Settings applied correctly?


## Local development

Install nextjs

    npm run dev


## Todo

 - is there a way to check the configuration at the server start up?
 - set up tls