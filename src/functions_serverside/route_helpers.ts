import { NextRequest } from "next/server"



export async function logTextParamsAndBody(text: string, request: NextRequest) {
    console.log(text)
    console.log(request.nextUrl.searchParams)

    let jsonString: string|undefined;
    try {
        jsonString = await request.json()
    } catch (_) { /* ignored */ }

    console.log("request.json() has been called and can't be called again:")
    console.log(jsonString ?? "no json in request body")
}


export async function makeCallAndMaybeLogError(url: string, method: string, body: object) {
    await fetch(url, {
        method: method,
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(body)
    })
        .catch(console.error)
}