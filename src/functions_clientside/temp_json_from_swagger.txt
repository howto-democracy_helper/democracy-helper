Post
{
  "proposal": {
    "text": "string",
    "projectId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "created": "2024-04-22T13:34:47.763Z",
    "lastModified": "2024-04-22T13:34:47.763Z"
  },
  "password": "string"
}

Put
{
  "newProposal": {
    "text": "string",
    "projectId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "created": "2024-04-22T13:35:07.567Z",
    "lastModified": "2024-04-22T13:35:07.567Z"
  },
  "oldId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "password": "string"
}

Delete
{
  "toBeDeletedId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "password": "string"
}

---

Get
projectId guid
proposalId guid as query params

Post
{
  "vote": {
    "typeName": "string",
    "comment": "string",
    "projectId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "proposalId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "created": "2024-04-22T13:35:35.216Z",
    "lastModified": "2024-04-22T13:35:35.216Z"
  },
  "password": "string"
}

Put
{
  "newVote": {
    "typeName": "string",
    "comment": "string",
    "projectId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "proposalId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "created": "2024-04-22T13:36:10.128Z",
    "lastModified": "2024-04-22T13:36:10.128Z"
  },
  "oldId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "password": "string"
}

Delete
{
  "toBeDeletedId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "projectId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "password": "string"
}



