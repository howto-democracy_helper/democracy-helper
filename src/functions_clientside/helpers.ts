import { Proposal } from "@/types/Proposal"
import { Vote } from "@/types/Vote"
import { Guid } from "guid-ts"
import { getProposals } from "./proposals_calls"



export function createProposal(
    projectId: string,
    text: string,
): Proposal {
    return {
        text: text,
        projectId: projectId,
        id: Guid.newGuid().toString(),
        created: new Date(),
        lastModified: new Date()
    }
}

export function createVote(
    typeName: string,
    comment: string,
    projectId: string,
    proposalId: string
): Vote {
    return {
        typeName: typeName,
        comment: comment,
        projectId: projectId,
        proposalId: proposalId,
        id: Guid.newGuid().toString(),
        created: new Date(),
        lastModified: new Date()
    }
}
