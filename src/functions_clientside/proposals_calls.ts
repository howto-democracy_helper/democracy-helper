import { createProposal } from "./helpers"

// GET
export async function getProposals(projectId: string) {
    if(!projectId)
        throw new Error("invalid args")

    return fetch(`api/proposals?projectId=${projectId}`)
        .then(res => res.json())
        .then(json => {
            if((json as any).status === 400) {
                console.log("fetch not throwing but bad request")
                return []
            }
            console.log("helper: fetched proposals successfully")
            return json
        })
        .catch(console.error)
}

// POST
export async function postProposal(projectId: string, text: string, password: string) {
    if(!projectId || !text || !password)
        throw new Error("invalid args")

    const request_body_object = {
        proposal: createProposal(projectId, text),
        password: password
    }

    return fetchFromProposalsWithMethodAndBody("POST", request_body_object)
}

// PUT
export async function putProposal(projectId: string, proposalId: string, text: string, password: string) {
    if(!projectId || !proposalId || !text || !password)
        throw new Error("invalid args")

    const request_body_object = {
        newProposal: createProposal(projectId, text),
        oldId: proposalId,
        password: password
    }

    return fetchFromProposalsWithMethodAndBody("PUT", request_body_object)
}

// DELETE
export async function deleteProposal(toBeDeletedId: string, password: string) {
    if(!toBeDeletedId || !password)
        throw new Error("invalid args")

    const request_body_object = {
        toBeDeletedId: toBeDeletedId,
        password: password
    }

    return fetchFromProposalsWithMethodAndBody("DELETE", request_body_object)
}

// HELPERS
async function fetchFromProposalsWithMethodAndBody(method: Method, body: object) {
    return fetch(`api/proposals`, {
        method: method,
        body: JSON.stringify(body)
    })
        .then(res => res.json())
        .then(json => {
            return json
        })
        .catch(console.error)
}

type Method = "POST" | "PUT" | "DELETE"