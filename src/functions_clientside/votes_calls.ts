
import { createProposal, createVote } from "./helpers"

// not used
// // GET
// export async function getVotes(projectId: string) {
//     if (projectId == undefined)
//         throw new Error("projectId undefined!")

//     return fetch(`api/votes?projectId=${projectId}`)
//         .then(res => res.json())
//         .then(json => {
//             console.log(json)
//             return json
//         })
// }

export async function getVotesForProposal(projectId: string, proposalId: string) {
    if (projectId == undefined)
        throw new Error("projectId undefined!")
    if (proposalId == undefined)
        throw new Error("proposalId undefined!")

    return fetch(`api/votes?projectId=${projectId}&proposalId=${proposalId}`)
        .then(res => res.json())
        .then(json => {
            if((json as any).status === 400) {
                console.log("fetch not throwing but bad request")
                return []
            }
            console.log("helper: fetched votes successfully")
            return json
        })
        .catch(console.error)
}

// POST
export async function postVote(
    typeName: string,
    comment: string,
    projectId: string,
    proposalId: string,
    password: string
) {
    if(!typeName || !comment || !projectId || !proposalId || !password)
        throw new Error("invalid args")

    const request_body_object = {
        vote: createVote(typeName, comment, projectId, proposalId),
        password: password
    }

    return fetchFromVotesWithMethodAndBody("POST", request_body_object)
}

// PUT
export async function putVote(
    typeName: string,
    comment: string,
    projectId: string,
    proposalId: string,
    voteId: string,
    password: string
) {
    if(!typeName || !comment || !projectId || !proposalId || !voteId || !password)
        throw new Error("invalid args")

    const request_body_object = {
        newVote: createVote(typeName, comment, projectId, proposalId),
        oldId: voteId,
        password: password
    }

    return fetchFromVotesWithMethodAndBody("PUT", request_body_object)
}

// DELETE
export async function deleteVote(toBeDeletedId: string, projectId: string, password: string) {
    if(!toBeDeletedId || !projectId || !password)
        throw new Error("invalid args")
    const request_body_object = {
        toBeDeletedId: toBeDeletedId,
        projectId: projectId,
        password: password
    }

    return fetchFromVotesWithMethodAndBody("DELETE", request_body_object)
}

// HELPERS
async function fetchFromVotesWithMethodAndBody(method: Method, body: object) {
    return fetch(`api/votes`, {
        method: method,
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(body)
    })
        .then(res => res.json())
        .then(json => {
            return json
        })
        .catch(console.error)
}

type Method = "POST" | "PUT" | "DELETE"