

import { projects } from "@root/settings/appsettings.json"
import Link from "next/link";

export default function Home() {

    return (
        <main className="main">

            <p>Please navigate to one of the following projects:</p>
            {projects.map(project => (
                <p key={project.id}>
                    <Link href={`/${project.name}`}>
                        {project.name}
                    </Link>
                </p>

            ))}
        </main>
    );
}

