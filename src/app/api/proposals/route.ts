
import { makeCallAndMaybeLogError } from "@/functions_serverside/route_helpers";
import { NextRequest } from "next/server";
import { backendUrl } from "@root/settings/appsettings.json"

export async function GET(request: NextRequest) {
    const projectId = request.nextUrl.searchParams.get("projectId")
    const url = `${backendUrl}/api/proposals?projectId=${projectId}`

    const returnedFromBackend: Response = await fetch(url)
        .then(res => res.json())
        .catch(console.error)
    return Response.json(returnedFromBackend)
}


export async function POST(request: NextRequest) {
    const requestBody = await request.json();
    const url = `${backendUrl}/api/proposals`

    await makeCallAndMaybeLogError(url, "POST", requestBody)
    // TODO solve this better. Return may not be necessary.
    return Response.json({});
}

export async function PUT(request: NextRequest) {
    const requestBody = await request.json();
    const url = `${backendUrl}/api/proposals`

    await makeCallAndMaybeLogError(url, "PUT", requestBody)
    return Response.json({});
}

export async function DELETE(request: NextRequest) {
    const requestBody = await request.json();
    const url = `${backendUrl}/api/proposals`

    await makeCallAndMaybeLogError(url, "DELETE", requestBody)
    return Response.json({});
}

