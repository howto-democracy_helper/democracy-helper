

import type { Metadata } from "next";
import { Inter } from "next/font/google";
import "./globals.css";
import GlobalContextWrapper from "@/components/GlobalContextWrapper";
import { projects } from "@root/settings/appsettings.json"
import Link from "next/link";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
    title: "Democracy Helper",
    description: "",
};

export default function RootLayout({
    children,
}: Readonly<{
    children: React.ReactNode;
}>) {

    return (
        <html lang="en">
            <body className={inter.className}>
                <main>
                    <h1>Democracy Helper</h1>
                    <hr />
                    {children}
                </main>
            </body>
        </html>
    );
}
