
import GlobalContextWrapper from "@/components/GlobalContextWrapper"
import Main from "@/components/Main"
import { projects } from "@root/settings/appsettings.json"
import Link from "next/link"
import { notFound } from "next/navigation"
import { ReactNode } from "react"





export default function ProjectPage({ params: { pagename } }: { params: { pagename: string } }) {

    const project = projects.find(p => p.name === pagename)

    if (!project) {
        // notFound()
        return <main>
            <p>Project not found</p>
            <Link href={`/`}>
                Show awailable projects
            </Link>
        </main>
    }

    return (
        <main>
            <p>We found the project</p>
            <p>{project.name}</p>
            <GlobalContextWrapper project={project}>
                <Main />
            </GlobalContextWrapper>
        </main>
    )
}