

export interface Project {
    id: string,
    name: string,
    voteTypeList: VoteType[]
}