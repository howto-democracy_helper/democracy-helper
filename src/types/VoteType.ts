
// capital to match the backend config convention
interface VoteType {
    name: string,
    description: string
}