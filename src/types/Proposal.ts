

export interface Proposal {
    text: string,
    projectId: string,
    id: string,
    created: Date,
    lastModified: Date
}