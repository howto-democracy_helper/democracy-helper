


export interface Vote {
    typeName: string,
    comment: string,
    projectId: string,
    proposalId: string,
    id: string,
    created: Date,
    lastModified: Date
}