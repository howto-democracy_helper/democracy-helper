import { Vote } from "@/types/Vote"
import HandleModalButton from "@/components/HandleModalButton";
import VotesUpdateInterface from "./modal_content/VotesUpdateInterface";
import VotesDeleteInterface from "./modal_content/VotesDeleteInterface";
import { DeleteIcon, UpdateIcon } from "@/icons";
import { useContext } from "react";
import { VoteContext } from "@/context/VoteContext";


function VoteComponent({ vote }: { vote: Vote }) {
    const { updateVotes } = useContext(VoteContext)

    return <div className="box vote_div">
        <div className="line">

            <div> {/* start of line */}
                <label>
                    TypeName
                    <div>{vote.typeName}</div>
                </label>
            </div>

            <div > {/* end of line */}
                <HandleModalButton
                    content={<VotesUpdateInterface vote={vote} updateVotes={updateVotes} />}
                    icon={UpdateIcon}
                    title="update vote" />
                <HandleModalButton
                    content={<VotesDeleteInterface toBeDeleted={vote} updateVotes={updateVotes} />}
                    icon={DeleteIcon}
                    title="delete vote" />
            </div>
        </div>
        <div className="line no-overflow">
            <label className="no-overflow">
                Comment
                <div>{vote.comment}</div>
            </label>

        </div>
    </div>
}


export default VoteComponent

