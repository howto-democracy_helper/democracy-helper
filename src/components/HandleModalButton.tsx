import { GlobalContext } from "@/context/GlobalContext";
import { ReactNode, useContext } from "react";
import { IconType } from "react-icons";

/**
 * pass in content=undefined to close the dialog modal
 */

function HandleModalButton({ content, icon, title }:{ content: ReactNode, icon: ReactNode, title?: string }) {
    const { modalContentSetter } = useContext(GlobalContext);

    return <button onClick={() => modalContentSetter(content)} title={title}>
        {icon}
    </button>
}


export default HandleModalButton

// https://docs.fontawesome.com/web/use-with/react/use-with