"use client"

import { FormEventHandler, useContext, useEffect, useRef, useState } from "react"
import { GlobalContext } from "@/context/GlobalContext";
import { postProposal } from "@/functions_clientside/proposals_calls";
import { ProposalContext } from "@/context/ProposalContext";


function ProposalsAddInterface({ projectId }: { projectId: string }) {
    const { modalContentSetter } = useContext(GlobalContext)
    const { updateProposals } = useContext(ProposalContext)

    const submit: FormEventHandler<HTMLFormElement> = async e => {
        e.preventDefault()
        const target = e.target as any

        const text = target.text.value
        const password = target.password.value
        await postProposal(projectId, text, password)
        
        modalContentSetter(undefined)
        updateProposals()
    }

    return <>
        <form onSubmit={submit}>

            <label htmlFor="t">Text:</label>
            <input id="t" name="text" type="text" autoFocus required />

            <label htmlFor="pw">Password:</label>
            <input id="pw" name="password" type="password" required />

            <div className="line-justify-end">
                <button type="submit">Add proposal</button>
            </div>
        </form>
    </>
}

export default ProposalsAddInterface