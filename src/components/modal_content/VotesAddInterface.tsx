"use client"

import { FormEventHandler, useContext, useEffect, useRef, useState } from "react"
import { GlobalContext } from "@/context/GlobalContext";
import { postVote } from "@/functions_clientside/votes_calls";


function VotesAddInterface({ projectId, proposalId, updateVotes }: { projectId: string, proposalId: string, updateVotes: () => Promise<void> }) {
    const { project, modalContentSetter } = useContext(GlobalContext);
    const voteTypesNameList = project.voteTypeList.map(e => e.name)

    const submit: FormEventHandler<HTMLFormElement> = async e => {
        e.preventDefault()
        const target = e.target as any

        const voteType = target.voteType.value
        const comment = target.comment.value
        const password = target.password.value
        await postVote(
            voteType,
            comment,
            projectId,
            proposalId,
            password)
        modalContentSetter(undefined)
        updateVotes()
    }

    return <>
        <form onSubmit={submit}>
            <label htmlFor="voteType">VoteType:</label>
            {voteTypesNameList.map(voteName =>
                <div key={voteName}>
                    <input key={voteName} name="voteType" type="radio" value={voteName} required />
                    <span>{voteName}</span>
                </div>
            )}

            <label htmlFor="t">Comment:</label>
            <input id="t" name="comment" type="text" autoFocus required />

            <label htmlFor="pw">Password:</label>
            <input id="pw" name="password" type="password" required />

            <div className="line-justify-end">
                <button type="submit">Add vote</button>
            </div>
        </form>
    </>
}

export default VotesAddInterface