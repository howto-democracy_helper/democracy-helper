"use client"

import { FormEventHandler, useContext, useEffect, useRef, useState } from "react"
import { deleteVote } from "@/functions_clientside/votes_calls";
import { GlobalContext } from "@/context/GlobalContext";
import { Vote } from "@/types/Vote";


function VotesDeleteInterface({ toBeDeleted, updateVotes }: { toBeDeleted: Vote, updateVotes: () => Promise<void> }) {
    const { modalContentSetter } = useContext(GlobalContext);

    const submit: FormEventHandler<HTMLFormElement> = async e => {
        e.preventDefault()
        const target = e.target as any

        const password = target.password.value
        await deleteVote(toBeDeleted.id, toBeDeleted.projectId, password)

        modalContentSetter(undefined)
        updateVotes()
    }

    return <>
        <form onSubmit={submit}>
            <label>
                Password:
                <input name="password" type="password" autoFocus required />
            </label>
            <div className="line-justify-end">
                <button type="submit">Delete vote</button>
            </div>
        </form >
    </>
}

export default VotesDeleteInterface