"use client"

import { FormEventHandler, useContext } from "react"
import { GlobalContext } from "@/context/GlobalContext";
import { putProposal } from "@/functions_clientside/proposals_calls";
import { Proposal } from "@/types/Proposal";
import { ProposalContext } from "@/context/ProposalContext";

function ProposalsUpdateInterface({ proposal }: { proposal: Proposal }) {
    const { modalContentSetter } = useContext(GlobalContext);
    const { updateProposals } = useContext(ProposalContext)

    const submit: FormEventHandler<HTMLFormElement> = async e => {
        e.preventDefault()
        const target = e.target as any

        const text = target.text.value
        const password = target.password.value
        await putProposal(proposal.projectId, proposal.id, text, password)

        modalContentSetter(undefined)
        updateProposals()
    }

    return <>
        <form onSubmit={submit}>

            <label htmlFor="t">Text:</label>
            <input id="t" name="text" type="text" defaultValue={proposal.text} autoFocus required />

            <label htmlFor="pw">Password:</label>
            <input id="pw" name="password" type="password" required />

            <div className="line-justify-end">
                <button type="submit">Update proposal</button>
            </div>
        </form>
    </>
}

export default ProposalsUpdateInterface