"use client"

import { FormEventHandler, useContext, useEffect, useRef, useState } from "react"
import { GlobalContext } from "@/context/GlobalContext";
import { deleteProposal, getProposals } from "@/functions_clientside/proposals_calls";
import { ProposalContext } from "@/context/ProposalContext";
import { Proposal } from "@/types/Proposal";


function ProposalsDeleteInterface({ toBeDeleted }: { toBeDeleted: Proposal }) {
    const { modalContentSetter } = useContext(GlobalContext);
    const { updateProposals } = useContext(ProposalContext)

    const submit: FormEventHandler<HTMLFormElement> = async e => {
        e.preventDefault()
        const target = e.target as any

        const password = target.password.value
        await deleteProposal(toBeDeleted.id, password)

        modalContentSetter(undefined)
        updateProposals()
    }

    return <>
        <form onSubmit={submit}>
            <label htmlFor="pw">Password:</label>
            <input id="pw" name="password" type="password" required autoFocus/>

            <div className="line-justify-end">
                <button type="submit">Delete proposal</button>
            </div>
        </form>
    </>
}

export default ProposalsDeleteInterface