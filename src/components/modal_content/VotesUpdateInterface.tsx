"use client"

import { FormEventHandler, KeyboardEventHandler, useContext, useEffect, useRef, useState } from "react"
import { GlobalContext } from "@/context/GlobalContext";
import { putVote } from "@/functions_clientside/votes_calls";
import { Vote } from "@/types/Vote";


function VotesUpdateInterface({ vote, updateVotes }: { vote: Vote, updateVotes: () => Promise<void> }) {
    const { project, modalContentSetter } = useContext(GlobalContext);
    const voteTypesNameList = project.voteTypeList.map(e => e.name)

    const submit: FormEventHandler<HTMLFormElement> = async e => {
        e.preventDefault()
        const target = e.target as any

        const voteType = target.voteType.value
        const comment = target.comment.value
        const password = target.password.value
        await putVote(
            voteType,
            comment,
            vote.projectId,
            vote.proposalId,
            vote.id,
            password
        )
        modalContentSetter(undefined)
        updateVotes()
    }

    return <>
        <form onSubmit={submit}>
            <label htmlFor="voteType">VoteType:</label>
            {voteTypesNameList.map(voteName =>
                <div key={voteName}>
                    {(voteName === vote.typeName) ?
                        <input type="radio" name="voteType" value={voteName} defaultChecked required /> :
                        <input type="radio" name="voteType" value={voteName} required />}
                    <span>{voteName}</span>
                </div>
            )}

            <label htmlFor="t">Comment:</label>
            <input id="t" name="comment" type="text" defaultValue={vote.comment} autoFocus required />

            <label htmlFor="pw">Password:</label>
            <input id="pw" name="password" type="password" required />

            <div className="line-justify-end">
                <button type="submit">Update vote</button>
            </div>
        </form>
    </>
}

export default VotesUpdateInterface