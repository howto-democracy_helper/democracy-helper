"use client"

import { GlobalContext } from "@/context/GlobalContext";
import { Project } from "@/types/Project";
import { ReactNode, useState } from "react";


function GlobalContextWrapper({
    project,
    children,
}: Readonly<{
    project: Project,
    children: React.ReactNode;
}>) {
    const [modalContent, modalContentSet] = useState<ReactNode | undefined>(undefined)
    const modalContentSetter = (undefinedToHideModal: ReactNode | undefined) => modalContentSet(undefinedToHideModal)
    return (
        <>
            <GlobalContext.Provider value={{ modalContent: modalContent, modalContentSetter: modalContentSetter, project: project }}>
                {children}
            </GlobalContext.Provider>
        </>
    );
}

export default GlobalContextWrapper