"use client"

import { getVotesForProposal } from "@/functions_clientside/votes_calls";
import { Proposal } from "@/types/Proposal";
import { Vote } from "@/types/Vote";
import { useCallback, useEffect, useRef, useState } from "react";
import VoteComponent from "./VoteComponent";
import HandleModalButton from "@/components/HandleModalButton";
import ProposalsUpdateInterface from "./modal_content/ProposalsUpdateInterface";
import ProposalsDeleteInterface from "./modal_content/ProposalsDeleteInterface";
import VotesAddInterface from "./modal_content/VotesAddInterface";
import { AddIcon, DeleteIcon, UpdateIcon } from "@/icons";
import { VoteContext } from "@/context/VoteContext";


function ProposalComponent({ proposal }: { proposal: Proposal }) {
    const [votes, votesSet] = useState<Vote[]>([])

    const fetchAndSetVotes = useCallback(async () => {
        const loadedVotes = await getVotesForProposal(proposal.projectId, proposal.id)
        votesSet(loadedVotes)
    }, [proposal.id, proposal.projectId])

    useEffect(() => {
        fetchAndSetVotes()
    }, [fetchAndSetVotes])

    return <div className="box proposal-div">
        <div className="line">

            <div> {/* start of line */}
                <label>
                    Text:
                    <div className="no-overflow">{proposal.text}</div>
                </label>
            </div>

            <div> {/* end of line */}
                <HandleModalButton
                    content={<ProposalsUpdateInterface proposal={proposal} />}
                    icon={UpdateIcon}
                    title="update proposal" />

                <HandleModalButton
                    content={<ProposalsDeleteInterface toBeDeleted={proposal} />}
                    icon={DeleteIcon}
                    title="delete proposal" />
            </div>

        </div>

        <VoteContext.Provider value={{ updateVotes: fetchAndSetVotes }}>
            <div className="votes-list">
                {votes && votes.map(v => <VoteComponent key={v.id} vote={v} />)}
            </div>

            <div className="line-justify-end">
                <HandleModalButton
                    content={<VotesAddInterface projectId={proposal.projectId} proposalId={proposal.id} updateVotes={fetchAndSetVotes}/>}
                    icon={AddIcon}
                    title="add vote for this proposal" />
            </div>
        </VoteContext.Provider>
    </div>
}

export default ProposalComponent


