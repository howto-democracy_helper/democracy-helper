"use client"

import { GlobalContext } from "@/context/GlobalContext"
import { ProposalContext } from "@/context/ProposalContext"
import { getProposals } from "@/functions_clientside/proposals_calls"
import { AddIcon } from "@/icons"
import { Proposal } from "@/types/Proposal"
import { useState, useContext, useRef, useEffect, useCallback } from "react"
import HandleModalButton from "./HandleModalButton"
import Modal from "./Modal"
import ProposalComponent from "./ProposalComponent"
import ProposalsAddInterface from "./modal_content/ProposalsAddInterface"

export default function Main() {
    const [proposals, proposalsSet] = useState<Proposal[]>([])
    const { project } = useContext(GlobalContext)

    const fetchAndSetProposals = useCallback(async () => {
        const loadedProposals = await getProposals(project.id)
        proposalsSet(() => loadedProposals)
    }, [project.id])

    useEffect(() => {
        fetchAndSetProposals()
    }, [fetchAndSetProposals])

    return (
        <ProposalContext.Provider value={{ updateProposals: fetchAndSetProposals }}>
            {proposals && proposals.map(p =>
                <ProposalComponent key={p.id} proposal={p} />
            )}
            <div className="line-justify-end">
                <HandleModalButton
                    content={<ProposalsAddInterface projectId={project.id} />}
                    icon={AddIcon}
                    title="add proposal" />
            </div>
            <hr />
            <Modal />
        </ProposalContext.Provider>
    );
}

