"use client"

import { GlobalContext } from "@/context/GlobalContext";
import { useContext, useEffect, useRef } from "react";
import HandleModalButton from "./HandleModalButton";
import { CloseIcon } from "@/icons";

function Modal() {
    const { modalContent } = useContext(GlobalContext);


    return (
        <>
            {modalContent && <dialog open>
                <div className="modal-background">
                    <div className="modal-box">
                        <div className="modal-delete-button-div">
                            <HandleModalButton content={undefined} icon={CloseIcon} title="close dialog window" />
                        </div>
                        <div className="modal-content">
                            {modalContent}
                        </div>
                    </div>
                </div>
            </dialog >}
        </>
    );
}

export default Modal;

// https://medium.com/@a.pirus/create-a-modal-that-can-be-opened-from-anywhere-in-next-js-13-36f6d0ce1bcf