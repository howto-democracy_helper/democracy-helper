"use client"

import React from "react"

export interface VoteContextType {
    updateVotes: () => Promise<void>
}

// these are just default values typically overridden in page
export const VoteContext = React.createContext<VoteContextType>({
    updateVotes: () => new Promise(resolve => resolve())
})