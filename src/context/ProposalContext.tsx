"use client"

import React from "react"

export interface ProposalContextType {
    updateProposals: () => void
}

// these are just default values typically overridden in page
export const ProposalContext = React.createContext<ProposalContextType>({
    updateProposals: () => {}
})