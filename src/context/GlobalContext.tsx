"use client"

import { Project } from "@/types/Project"
import React, { ReactNode } from "react"

export interface GlobalContextType {
    modalContent: React.ReactNode | undefined
    modalContentSetter: (undefinedToHideModal: ReactNode | undefined) => void
    project: Project
}

// these are just default values typically overridden in the component GlobalContextWrapper
export const GlobalContext = React.createContext<GlobalContextType>({
    modalContent: <></>, // set to undefined if modal should not show
    modalContentSetter: node => {},
    project: {
        id: "",
        name: "",
        voteTypeList: []
    },
})