import { MdAdd, MdDelete, MdModeEdit, MdClose } from "react-icons/md";

// more at https://react-icons.github.io/react-icons/icons/md/

export const AddIcon = <MdAdd />;
export const UpdateIcon = <MdModeEdit />;
export const DeleteIcon = <MdDelete />;
export const CloseIcon = <MdClose />;