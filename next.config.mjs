/** @type {import('next').NextConfig} */
import appSettings from "./settings/appsettings.json" assert { type: "json" };


const nextConfig = {
    output: "standalone",
    basePath: appSettings.basePath,
    trailingSlash: false,
};

export default nextConfig;
